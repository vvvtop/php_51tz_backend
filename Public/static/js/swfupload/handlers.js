
function fileQueued(file) {
    /* 
    if(/\[\]$/.test(this.customSettings.hiddenFieldName)){
        this.uploadType = 'multi';
    }else{
        this.uploadType = 'single';
    }

    if(this.uploadType == 'single' && $('#'+this.customSettings.progressTarget).children().length > 0){
        alert('只允许上传一个文件，请删除原有文件再上传');
        return false;
    }
    */

    try {
        var progress = new FileProgress(file, this.customSettings.progressTarget);
        progress.setStatus("准备上传...");
        progress.toggleCancel(true, this);
        progress.delFile(this);
    } catch (ex) {
        this.debug(ex);
    }

}

function fileQueueError(file, errorCode, message) {
    try {
        switch (errorCode) {
            case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
                alert("文件大小不得超过10MB");
                break;
            case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
                alert("无法上传空文件");
                break;
            case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
                alert("不能上传此类型文件");
                break;
            case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
                alert("附件最多只能添加5个");
                break;
            default:
                alert(message);
                break;
        }
    } catch (ex) {
        this.debug(ex);
    }
}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
    try {
        if (this.getStats().files_queued > 0) {
            this.startUpload();
        }
    } catch (ex)  {
        this.debug(ex);
    }
}

function uploadStart(file) {
    try {
        var progress = new FileProgress(file, this.customSettings.progressTarget);
        progress.setStatus("上传中...");
        progress.toggleCancel(true, this);
    }
    catch (ex) {
    }

    return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal) {

    try {
        var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);

        var progress = new FileProgress(file, this.customSettings.progressTarget);
        progress.setProgress(percent);
    } catch (ex) {
        this.debug(ex);
    }
}

function uploadSuccess(file, serverData) {
    try {
        var progress = new FileProgress(file, this.customSettings.progressTarget);
        serverData = JSON.parse(serverData);
        if (serverData.status == "1") {

            progress.setComplete(file, serverData, this.customSettings.hiddenFieldName);
            progress.setStatus("上传完成");
            progress.toggleCancel(false);
        } else {
            progress.setError();
            progress.setStatus(serverData.message);
            progress.toggleCancel(false);
        }
    } catch (ex) {
        this.debug(ex);
    }
}

function uploadComplete(file) {
    try {
        if (this.getStats().files_queued > 0) {
            this.startUpload();
        }
    } catch (ex) {
        this.debug(ex);
    }

}

function uploadError(file, errorCode, message) {
    try {
        var progress = new FileProgress(file, this.customSettings.progressTarget);
        progress.setError();
        progress.toggleCancel(false);

        switch (errorCode) {
            case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
                progress.setStatus("上传取消");
                progress.setCancelled();
                break;
            case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
                progress.setStatus("上传终止");
                break;
            default:
                progress.setStatus("上传错误:"+errorCode);
                break;
        }
    } catch (ex) {
        this.debug(ex);
    }
}




/*上传进度类*/
function FileProgress(file, targetID) {
    this.fileProgressID = file.id;
    this.fileName = file.name;

    this.opacity = 100;
    this.height = 0;
    this.uploaded = false;


    this.fileProgressWrapper = document.getElementById(this.fileProgressID);
    if (!this.fileProgressWrapper) {
        this.fileProgressWrapper = document.createElement("div");
        this.fileProgressWrapper.className = "progressWrapper";
        this.fileProgressWrapper.id = this.fileProgressID;

        this.fileProgressElement = document.createElement("div");
        this.fileProgressElement.className = "progressContainer";

        //文件名和大小
        var progressText = document.createElement("span");
        progressText.className = "progressPhoto";
        progressText.appendChild(document.createTextNode(file.name));
        var sizeText = document.createElement("span");
        sizeText.style.color = "#798699";
        sizeText.innerHTML = " ("+getFormatSize(file.size)+")";
        progressText.appendChild(sizeText);

        //进度条
        var progressBar = document.createElement("span");
        progressBar.className = "progressBarInProgress";
        var progressBarProgress = document.createElement("span");
        progressBarProgress.className = "progressBarProgress";
        progressBarProgress.innerHTML = "&nbsp;";
        progressBar.appendChild(progressBarProgress);

        //提示文字
        var progressStatus = document.createElement("span");
        progressStatus.className = "progressBarStatus";
        progressStatus.innerHTML = "&nbsp;";

        //取消按钮
        var progressCancel = document.createElement("a");
        progressCancel.className = "progressCancel";
        progressCancel.href = "javascript:;";
        progressCancel.style.display = "none";
        progressCancel.appendChild(document.createTextNode("取消"));

        //删除按钮
        var progressDel = document.createElement("a");
        progressDel.className = "progressDel";
        progressDel.href = "javascript:;";
        progressDel.style.display = "none";
        progressDel.appendChild(document.createTextNode("删除"));

        this.fileProgressElement.appendChild(progressText);
        this.fileProgressElement.appendChild(progressBar);
        this.fileProgressElement.appendChild(progressStatus);
        this.fileProgressElement.appendChild(progressCancel);
        this.fileProgressElement.appendChild(progressDel);

        this.fileProgressWrapper.appendChild(this.fileProgressElement);

        document.getElementById(targetID).appendChild(this.fileProgressWrapper);
    } else {
        this.fileProgressElement = this.fileProgressWrapper.firstChild;
        this.reset();
    }

    function getFormatSize(size){
        var formatSize;
        if(!size){
            formatSize = '';
        }
        else if(size < 1024*1024){
            formatSize = (size/1024).toFixed(1) + 'KB';
        }
        else{
            formatSize = (size/1024/1024).toFixed(1) +'MB';
        }
        return formatSize;
    }
}



FileProgress.prototype.reset = function () {
    this.fileProgressElement.childNodes[1].className = "progressBarInProgress";
    this.fileProgressElement.childNodes[1].style.width = "100px";

    this.fileProgressElement.childNodes[2].innerHTML = "&nbsp;";
    this.fileProgressElement.childNodes[2].className = "progressBarStatus";
};

FileProgress.prototype.setProgress = function (percentage) {
    this.fileProgressElement.childNodes[1].className = "progressBarInProgress";
    this.fileProgressElement.childNodes[1].childNodes[0].style.width = percentage + "%";

    if(parseInt(percentage) == 100){
        this.fileProgressElement.childNodes[2].innerHTML = "上传完成，正在处理...";
        this.fileProgressElement.childNodes[3].style.display = "none";
    }else{
        this.fileProgressElement.childNodes[2].innerHTML = percentage + "%";
    }
};

FileProgress.prototype.setComplete = function (flie, serverData, hiddenField) {
    //设置上传成功标识
    this.uploaded = true;
    //隐藏进度条
    this.fileProgressElement.childNodes[1].style.display = "none";
    this.fileProgressElement.childNodes[2].style.display = "none";
    this.fileProgressElement.childNodes[4].style.display = "inline-block";

    //设置隐藏域，如果以[]结尾，则为数组，创建；否则更新
    if(/\[\]$/.test(hiddenField)){
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = hiddenField;
        input.value = serverData.result.photo;
        this.fileProgressElement.appendChild(input);
    }else{
        $("#"+hiddenField).val(serverData.result.photo);
    }

    $('#'+this.fileProgressID).find('.progressPhoto').html('<img src="http://img.51tz.com/'+serverData.result.photo+'">');
};
FileProgress.prototype.setError = function () {
    this.fileProgressElement.childNodes[1].className = "progressBarError";
    this.fileProgressElement.childNodes[4].style.display = "inline-block";
};
FileProgress.prototype.setCancelled = function () {
    this.fileProgressElement.className = "progressContainer";
    this.fileProgressElement.childNodes[1].className = "progressBarError";
    this.fileProgressElement.childNodes[4].style.display = "inline-block";
};
FileProgress.prototype.setStatus = function (status) {
    this.fileProgressElement.childNodes[2].innerHTML = status;
};

//取消上传
FileProgress.prototype.toggleCancel = function (show, swfUploadInstance) {
    this.fileProgressElement.childNodes[3].style.display = show ? "inline-block" : "none";
    if (swfUploadInstance) {
        var fileID = this.fileProgressID;
        this.fileProgressElement.childNodes[3].onclick = function () {
            swfUploadInstance.cancelUpload(fileID);
            return false;
        };
    }
};

//删除上传
FileProgress.prototype.delFile = function (swfUploadInstance) {
    var self = this;
    if (swfUploadInstance) {
        var fileID = this.fileProgressID;
        $('#'+fileID).find('.progressDel').bind('click', function(){
            var upSta = swfUploadInstance.getStats();
            if(self.uploaded){
                upSta.successful_uploads--;
            }else{
                upSta.upload_errors--;
            }
            swfUploadInstance.setStats(upSta);

            $(this).parent().parent().remove();
            return false;    
        });
    }
};



var imageUploader = function(listDom, fieldDom, upDom, params){
    if(listDom == undefined || fieldDom == undefined || upDom == undefined){
        alert('请传入列表DOM和隐藏字段DOM名称');
        return false;
    }
    var uploader = new SWFUpload({
        upload_url: "http://img.51tz.com/upload.php",
        flash_url : "/Public/static/js/swfupload/swfupload.swf",

        file_size_limit : "10 MB",
        file_types : "*.jpg;*.jpeg;*.png;*.gif",
        file_types_description : "图片文件",
        file_upload_limit : 4,
        file_queue_limit : 4,

        file_queued_handler : fileQueued,
        file_queue_error_handler : fileQueueError,
        file_dialog_complete_handler : fileDialogComplete,
        upload_start_handler : uploadStart,
        upload_progress_handler : uploadProgress,
        upload_error_handler : uploadError,
        upload_success_handler : uploadSuccess,
        upload_complete_handler : uploadComplete,

        button_image_url : "/Public/static/js/swfupload/images/SmallSpyGlassWithTransperancy_17x18.png",
        button_placeholder_id : upDom,
        button_width: 70,
        button_height: 18,
        button_text : '选择图片',
        button_text_style : '',
        button_text_top_padding: 2,
        button_text_left_padding: 18,
        button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
        button_cursor: SWFUpload.CURSOR.HAND,
        debug: false,

        custom_settings : {
            progressTarget: listDom,
            hiddenFieldName: fieldDom,  //如果以[]结尾，代表多图
        },
        post_params: params || {}
    });
    
    return uploader;
}
