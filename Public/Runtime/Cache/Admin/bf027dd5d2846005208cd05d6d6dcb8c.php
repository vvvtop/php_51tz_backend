<?php if (!defined('THINK_PATH')) exit();?>
<table id="member_typelist_datagrid" class="easyui-datagrid" data-options='<?php $dataOptions = array_merge(array ( 'border' => false, 'fit' => true, 'fitColumns' => true, 'rownumbers' => true, 'singleSelect' => true, 'pagination' => true, 'pageList' => array ( 0 => 10, 1 => 15, 2 => 20, 3 => 30, 4 => 50, 5 => 80, 6 => 100, ), 'pageSize' => '15', ), $datagrid["options"]);if(isset($dataOptions['toolbar']) && substr($dataOptions['toolbar'],0,1) != '#'): unset($dataOptions['toolbar']); endif; echo trim(json_encode($dataOptions), '{}[]').((isset($datagrid["options"]['toolbar']) && substr($datagrid["options"]['toolbar'],0,1) != '#')?',"toolbar":'.$datagrid["options"]['toolbar']:null); ?>' style=""><thead><tr><?php if(is_array($datagrid["fields"])):foreach ($datagrid["fields"] as $key=>$arr):if(isset($arr['formatter'])):unset($arr['formatter']);endif;echo "<th data-options='".trim(json_encode($arr), '{}[]').(isset($datagrid["fields"][$key]['formatter'])?",\"formatter\":".$datagrid["fields"][$key]['formatter']:null)."'>".$key."</th>";endforeach;endif; ?></tr></thead></table>

<!-- 添加分类 -->
<div id="member_typelist_add_dialog" class="easyui-dialog" title="添加分类" data-options="modal:true,closed:true,iconCls:'icons-application-application_add',buttons:[{text:'确定',iconCls:'icons-other-tick',handler:function(){$('#member_typelist_add_dialog_form').submit();}},{text:'取消',iconCls:'icons-arrow-cross',handler:function(){$('#member_typelist_add_dialog').dialog('close');}}]" style="width:480px;height:300px;"></div>

<!-- 编辑分类 -->
<div id="member_typelist_edit_dialog" class="easyui-dialog" title="编辑分类" data-options="modal:true,closed:true,iconCls:'icons-application-application_edit',buttons:[{text:'确定',iconCls:'icons-other-tick',handler:function(){$('#member_typelist_edit_dialog_form').submit();}},{text:'取消',iconCls:'icons-arrow-cross',handler:function(){$('#member_typelist_edit_dialog').dialog('close');}}]" style="width:480px;height:300px;"></div>

<script type="text/javascript">
	var member_typelist_datagrid_toolbar = [
		{ text: '添加分类', iconCls: 'icons-arrow-add', handler: memberTypeListAdd },
		{ text: '刷新', iconCls: 'icons-arrow-arrow_refresh', handler: memberTypeListRefresh },
		{ text: '排序', iconCls: 'icons-arrow-arrow_down', handler: memberTypeListOrder }
	];
	//排序格式化
	function memberTypeListOrderFormatter(val, arr){
		return '<input class="member_typelist_order_input" type="text" name="order['+arr['typeid']+']" value="'+val+'" size="2" style="text-align:center" />';
	}
	//状态格式化
	function memberTypeListStateFormatter(val){
		return val == 1 ? '<font color="red">未启用</font>' : '已启用';
	}
	//操作格式化
	function memberTypeListOperateFormatter(val){
		var btn = [];
		if(val == 1){
			btn.push('编辑');
			btn.push('删除');
		}else{
			btn.push('<a href="javascript:void(0);" onclick="memberTypeListEdit('+val+')">编辑</a>');
			btn.push('<a href="javascript:void(0);" onclick="memberTypeListDelete('+val+')">删除</a>');
		}
		return btn.join(' | ');
	}
	//刷新
	function memberTypeListRefresh(){
		$('#member_typelist_datagrid').datagrid('reload');
	}
	//添加
	function memberTypeListAdd(){
		$('#member_typelist_add_dialog').dialog({href:'<?php echo U('Member/typeAdd');?>'});
		$('#member_typelist_add_dialog').dialog('open');
	}
	//编辑
	function memberTypeListEdit(id){
		if(typeof(id) !== 'number'){
			$.messager.alert('提示信息', '未选择分类', 'error');
			return false;
		}
		var url = '<?php echo U('Member/typeEdit');?>';
		url += url.indexOf('?') != -1 ? '&id='+id : '?id='+id;
		$('#member_typelist_edit_dialog').dialog({href:url});
		$('#member_typelist_edit_dialog').dialog('open');
	}
	//删除
	function memberTypeListDelete(id){
		if(typeof(id) !== 'number'){
			$.messager.alert('提示信息', '未选择分类', 'error');
			return false;
		}
		$.messager.confirm('提示信息', '确定要删除吗？', function(result){
			if(!result) return false;
			$.post('<?php echo U('Member/typeDelete');?>', {id: id}, function(res){
				if(!res.status){
					$.messager.alert('提示信息', res.info, 'error');
				}else{
					$.messager.alert('提示信息', res.info, 'info');
					memberTypeListRefresh();
				}
			}, 'json');
		});
	}
	//排序
	function memberTypeListOrder(){
		$.post('<?php echo U('Member/typeOrder');?>', $('.member_typelist_order_input').serialize(), function(res){
			if(!res.status){
				$.messager.alert('提示信息', res.info, 'error');
			}else{
				$.messager.alert('提示信息', res.info, 'info');
				memberTypeListRefresh();
			}
		}, 'json');
	}
</script>