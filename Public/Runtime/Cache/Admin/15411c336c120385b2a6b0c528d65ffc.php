<?php if (!defined('THINK_PATH')) exit();?><script type="text/javascript">
$(function(){
    $.formValidator.initConfig({
        formID:"city_city_edit_dialog_form",
        onError:function(msg){},
        onSuccess:cityEditDialogFormSubmit,
        submitAfterAjaxPrompt:'有数据正在异步验证，请稍等...',
        inIframe:true
    });
});  
function cityEditDialogFormSubmit(){
    $.post('<?php echo U('City/editCityFrame', array('id'=>$info['id']));?>', $("#city_city_edit_dialog_form").serialize(), function(res){
        if(!res.status){
            $.messager.alert('提示信息', res.info, 'error');
        }else{
            $.messager.alert('提示信息', res.info, 'info');
            $('#city_city_edit_dialog').dialog('close');
        }
    });
} 
</script>
<form id="city_city_edit_dialog_form" style="padding:10px;">
	<table width="100%" cellpadding="2">
		<tr>
			<td width="80">seo标题：</td>
			<td width="450"><textarea name="info[seo_title]" style="width:100%;height:40px"><?php echo ($info["seo_title"]); ?></textarea></td>
		</tr>
		<tr>
			<td>seo关键字：</td>
			<td><textarea name="info[seo_keywords]" style="width:100%;height:40px"><?php echo ($info["seo_keywords"]); ?></textarea></td>
		</tr>
		<tr>
			<td>seo描述：</td>
			<td><textarea name="info[seo_description]" style="width:100%;height:50px"><?php echo ($info["seo_description"]); ?></textarea></td>
		</tr>
        <tr>
            <td>流量统计脚本：</td>
            <td><textarea name="info[statistics_script]" style="width:100%;height:50px"><?php echo ($info["statistics_script"]); ?></textarea></td>
        </tr>
		<tr>
			<td>新浪微博：</td>
			<td><input type="text" name="info[sina_weibo]" value="<?php echo ($info["sina_weibo"]); ?>" style="width:100%" /></td>
		</tr>
		<tr>
			<td>客服：</td>
			<td><label><input type="checkbox" name="info[show_customer]" value="1" <?php if(($info["show_customer"]) == "1"): ?>checked<?php endif; ?> /> 显示</label></td>
		</tr>
	</table>
</form>