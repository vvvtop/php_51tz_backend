<?php if (!defined('THINK_PATH')) exit();?>
<table id="system_filelist_treegrid" class="easyui-treegrid" data-options='<?php $dataOptions = array_merge(array ( 'border' => false, 'fit' => true, 'fitColumns' => true, 'rownumbers' => true, 'singleSelect' => true, 'animate' => true, ), $treegrid["options"]);if(isset($dataOptions['toolbar']) && substr($dataOptions['toolbar'],0,1) != '#'): unset($dataOptions['toolbar']); endif; echo trim(json_encode($dataOptions), '{}[]').((isset($treegrid["options"]['toolbar']) && substr($treegrid["options"]['toolbar'],0,1) != '#')?',"toolbar":'.$treegrid["options"]['toolbar']:null); ?>' style=""><thead><tr><?php if(is_array($treegrid["fields"])):foreach ($treegrid["fields"] as $key=>$arr):if(isset($arr['formatter'])):unset($arr['formatter']);endif;echo "<th data-options='".trim(json_encode($arr), '{}[]').(isset($treegrid["fields"][$key]['formatter'])?",\"formatter\":".$treegrid["fields"][$key]['formatter']:null)."'>".$key."</th>";endforeach;endif; ?></tr></thead></table>

<!-- 查看文件 -->
<div id="system_filelist_view_dialog" class="easyui-dialog word-wrap" title="查看文件" data-options="modal:true,closed:true,resizable:true,maximizable:true,iconCls:'icons-application-application_view_detail',buttons:[{text:'关闭',iconCls:'icons-arrow-cross',handler:function(){$('#system_filelist_view_dialog').dialog('close');}}]" style="width:500px;height:360px;line-height:1.5"></div>


<script type="text/javascript">
//操作
function systemFileOperateFormatter(val, arr){
	var button = [];
	if(arr['type'] == 'dir'){
		button.push('查看');
		button.push('删除');
	}else{
		button.push('<a href="javascript:;" onclick="systemFileView(\''+arr.name+'\')">查看</a>');
		button.push('<a href="javascript:;" onclick="systemFileDelete(\''+arr.name+'\')">删除</a>');
	}
	return button.join(' | ');
}
//查看文件
function systemFileView(filename){
	var id = 'system_filelist_view_dialog';
	var url = '<?php echo U('System/fileView');?>';
	url += url.indexOf('?') != -1 ? '&filename='+filename : '?filename='+filename;
	$('#'+id).dialog({href: url});
	$('#'+id).dialog('open');
}
//删除文件
function systemFileDelete(filename){
	if(typeof(filename) !== 'string'){
		$.messager.alert('提示信息', '未选择文件', 'error');
		return false;
	}
	$.messager.confirm('提示信息', '确定要删除吗？', function(result){
		if(!result) return false;
		$.post('<?php echo U('System/fileDelete');?>', {filename: filename}, function(res){
			if(!res.status){
				$.messager.alert('提示信息', res.info, 'error');
			}else{
				$.messager.alert('提示信息', res.info, 'info');
				$('#system_filelist_treegrid').treegrid('reload');
			}
		}, 'json');
	});
}
</script>