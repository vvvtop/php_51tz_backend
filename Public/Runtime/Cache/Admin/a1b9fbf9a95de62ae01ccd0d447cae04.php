<?php if (!defined('THINK_PATH')) exit();?><script type="text/javascript">

$(function(){
    $.formValidator.initConfig({
        formID:"city_city_add_dialog_form",
        onError:function(msg){},
        onSuccess:cityCityAddDialogFormSubmit,
        submitAfterAjaxPrompt:'有数据正在异步验证，请稍等...',
        inIframe:true
    });
    $("#city_city_add_dialog_form_cityname").formValidator({
        onShow:"请输入城市名称",
        onFocus:"城市名称不能为空",
        onCorrect:"输入正确"
    }).inputValidator({
        min:1,
        empty:{leftEmpty:false,rightEmpty:false,emptyError:'城市名称不能有空格'},
        onError:"城市名称不能为空"
    });

    $("#city_city_add_dialog_form_domain").formValidator({
        onShow: "二级域名用小写字母标识",
        onFocus: "二级域名用小写字母标识",
        onCorrect: "填写正确"
    }).regexValidator({
        regExp: "^([a-z]+)$",
        onError: "二级域名格式填写错误"
    });

    $("#city_city_add_dialog_form_pinyin").formValidator({
        onShow: "小写全拼，字之间用英文空格隔开",
        onFocus: "小写全拼，字之间用英文空格隔开",
        onCorrect: "填写正确"
    }).regexValidator({
        regExp: "^([a-z]+)([a-z ]+)([a-z]+)$",
        onError: "拼音格式填写错误"
    });

    $("#city_city_add_dialog_form_firstchar").formValidator({
        onShow: "拼音首字母(小写)",
        onFocus: "拼音首字母(小写)",
        onCorrect: "填写正确"
    }).regexValidator({
        regExp: "^([a-z]{1})$",
        onError: "首字母格式填写错误"
    });
});  
function cityCityAddDialogFormSubmit(){
    $.post('<?php echo U('City/cityAddFrame');?>', $("#category_category_add_dialog_form").serialize(), function(res){
        if(!res.status){
            $.messager.alert('提示信息', res.info, 'error');
        }else{
            $.messager.alert('提示信息', res.info, 'info');
            $('#city_city_add_dialog').dialog('close');
            cityCityListRefresh();
        }
    })
}
</script>
<form id="city_city_add_dialog_form" style="padding:10px;">
<table width="100%">
    <tr>
        <td width="80">城市名称：</td>
        <td width="160"><input id="city_city_add_dialog_form_cityname" name="info[city_name]" type="text" style="width:160px;height:22px" /></td>
        <td><div id="city_city_add_dialog_form_citynameTip"></div></td>
    </tr>
    <tr>
        <td>二级域名：</td>
        <td><input id="city_city_add_dialog_form_domain" name="info[domain]" type="text" style="width:160px;height:22px" /></td>
        <td><div id="city_city_add_dialog_form_domainTip"></div></td>
    </tr>
    <tr>
        <td>拼音：</td>
        <td><input id="city_city_add_dialog_form_pinyin" name="info[pinyin]" type="text" style="width:160px;height:22px" /></td>
        <td><div id="city_city_add_dialog_form_pinyinTip"></div></td>
    </tr>
    <tr>
        <td>首字母：</td>
        <td><input id="city_city_add_dialog_form_firstchar" name="info[first_char]" type="text" style="width:50px;height:22px" /></td>
        <td><div id="city_city_add_dialog_form_firstcharTip"></div></td>
    </tr>
</table>
</form>