<?php if (!defined('THINK_PATH')) exit();?>
<table id="member_memberlist_datagrid" class="easyui-datagrid" data-options='<?php $dataOptions = array_merge(array ( 'border' => false, 'fit' => true, 'fitColumns' => true, 'rownumbers' => true, 'singleSelect' => true, 'pagination' => true, 'pageList' => array ( 0 => 10, 1 => 15, 2 => 20, 3 => 30, 4 => 50, 5 => 80, 6 => 100, ), 'pageSize' => '15', ), $datagrid["options"]);if(isset($dataOptions['toolbar']) && substr($dataOptions['toolbar'],0,1) != '#'): unset($dataOptions['toolbar']); endif; echo trim(json_encode($dataOptions), '{}[]').((isset($datagrid["options"]['toolbar']) && substr($datagrid["options"]['toolbar'],0,1) != '#')?',"toolbar":'.$datagrid["options"]['toolbar']:null); ?>' style=""><thead><tr><?php if(is_array($datagrid["fields"])):foreach ($datagrid["fields"] as $key=>$arr):if(isset($arr['formatter'])):unset($arr['formatter']);endif;echo "<th data-options='".trim(json_encode($arr), '{}[]').(isset($datagrid["fields"][$key]['formatter'])?",\"formatter\":".$datagrid["fields"][$key]['formatter']:null)."'>".$key."</th>";endforeach;endif; ?></tr></thead></table>

<!-- 添加用户 -->
<div id="member_memberlist_add_dialog" class="easyui-dialog" title="添加用户" data-options="modal:true,closed:true,iconCls:'icons-application-application_add',buttons:[{text:'确定',iconCls:'icons-other-tick',handler:function(){$('#member_memberlist_add_dialog_form').submit();}},{text:'取消',iconCls:'icons-arrow-cross',handler:function(){$('#member_memberlist_add_dialog').dialog('close');}}]" style="width:480px;height:260px;"></div>

<!-- 编辑用户 -->
<div id="member_memberlist_edit_dialog" class="easyui-dialog" title="编辑用户" data-options="modal:true,closed:true,iconCls:'icons-application-application_edit',buttons:[{text:'确定',iconCls:'icons-other-tick',handler:function(){$('#member_memberlist_edit_dialog_form').submit();}},{text:'取消',iconCls:'icons-arrow-cross',handler:function(){$('#member_memberlist_edit_dialog').dialog('close');}}]" style="width:480px;height:260px;"></div>

<script type="text/javascript">
var member_memberlist_datagrid_toolbar = [
	{ text: '添加用户', iconCls: 'icons-arrow-add', handler: memberMemberAdd },
	{ text: '刷新', iconCls: 'icons-arrow-arrow_refresh', handler: memberMemberRefresh }
];
//时间格式化
function memberMemberListTimeFormatter(val){
	return val != '1970-01-01 08:00:00' ? val : '';
}
//来源格式化
function memberMemberListSourceFormatter(val){
	return val ? val : '本站';
}
//操作格式化
function memberMemberListOperateFormatter(val){
	var btn = [];
	btn.push('<a href="javascript:;" onclick="memberMemberEdit('+val+')">编辑</a>');
	btn.push('<a href="javascript:;" onclick="memberMemberDelete('+val+')">删除</a>');
	return btn.join(' | ');
}

//刷新
function memberMemberRefresh(){
	$('#member_memberlist_datagrid').datagrid('reload');
}
//添加
function memberMemberAdd(){
	$('#member_memberlist_add_dialog').dialog({href:'<?php echo U('Member/memberAdd');?>'});
	$('#member_memberlist_add_dialog').dialog('open');
}
//编辑
function memberMemberEdit(id){
	if(typeof(id) !== 'number'){
		$.messager.alert('提示信息', '未选择用户', 'error');
		return false;
	}
	var url = '<?php echo U('Member/memberEdit');?>';
	url += url.indexOf('?') != -1 ? '&id='+id : '?id='+id;
	$('#member_memberlist_edit_dialog').dialog({href:url});
	$('#member_memberlist_edit_dialog').dialog('open');
}
//删除
function memberMemberDelete(id){
	if(typeof(id) !== 'number'){
		$.messager.alert('提示信息', '未选择用户', 'error');
		return false;
	}
	$.messager.confirm('提示信息', '确定要删除吗？', function(result){
		if(!result) return false;
		$.post('<?php echo U('Member/memberDelete');?>', {id: id}, function(res){
			if(!res.status){
				$.messager.alert('提示信息', res.info, 'error');
			}else{
				$.messager.alert('提示信息', res.info, 'info');
				memberMemberRefresh();
			}
		}, 'json');
	});
}
</script>