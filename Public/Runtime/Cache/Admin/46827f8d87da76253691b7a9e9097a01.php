<?php if (!defined('THINK_PATH')) exit();?>
<table id="coupon_couponlist_datagrid" class="easyui-datagrid" data-options='<?php $dataOptions = array_merge(array ( 'border' => false, 'fit' => true, 'fitColumns' => true, 'rownumbers' => true, 'singleSelect' => true, 'pagination' => true, 'pageList' => array ( 0 => 10, 1 => 15, 2 => 20, 3 => 30, 4 => 50, 5 => 80, 6 => 100, ), 'pageSize' => '15', ), $datagrid["options"]);if(isset($dataOptions['toolbar']) && substr($dataOptions['toolbar'],0,1) != '#'): unset($dataOptions['toolbar']); endif; echo trim(json_encode($dataOptions), '{}[]').((isset($datagrid["options"]['toolbar']) && substr($datagrid["options"]['toolbar'],0,1) != '#')?',"toolbar":'.$datagrid["options"]['toolbar']:null); ?>' style=""><thead><tr><?php if(is_array($datagrid["fields"])):foreach ($datagrid["fields"] as $key=>$arr):if(isset($arr['formatter'])):unset($arr['formatter']);endif;echo "<th data-options='".trim(json_encode($arr), '{}[]').(isset($datagrid["fields"][$key]['formatter'])?",\"formatter\":".$datagrid["fields"][$key]['formatter']:null)."'>".$key."</th>";endforeach;endif; ?></tr></thead></table>

<div id="coupon_couponlist_datagrid_toolbar" style="padding:1px;height:auto">
    <div>
        <a href="javascript:;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icons-arrow-add'" onclick="addCoupon()">添加优惠券</a>
        <a href="javascript:;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icons-other-delete'" onclick="couponDelete()">删除所选</a>
        <a href="javascript:;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icons-arrow-arrow_up'" onclick="couponDisplayOrder()">排序设置</a>
    </div>
</div>

<!-- 优惠券编辑 -->
<div id="coupon_coupon_edit_dialog" class="easyui-dialog" title="编辑优惠券" data-options="modal:true,closed:true,iconCls:'icons-application-application_edit',buttons:[{text:'确定',iconCls:'icons-other-tick',handler:function(){$('#coupon_coupon_dialog_form').submit();}},{text:'取消',iconCls:'icons-arrow-cross',handler:function(){$('#coupon_coupon_edit_dialog').dialog('close');}}]" style="width:550px;height:370px;"></div>

<script type="text/javascript">
var coupon_couponlist_datagrid_id = 'coupon_couponlist_datagrid';
//排序格式化
function displayOrderFormat(val, arr){
    return '<input class="coupon_gridlist_order_input" type="text" name="order['+arr['id']+']" value="'+ val +'" size="2" style="text-align:center">';
}
//状态格式化
function statusFormat(val){
    return val == 1 ? '<font color="green">可用</font>' : '<font color="red">不可用</font>';
}
//操作格式化
function operateFormat(id){
    var btn = [];
    btn.push('<a href="javascript:;" onclick="couponEdit('+id+')">编辑</a>');
    btn.push('<a href="javascript:;" onclick="couponDelete('+id+')">删除</a>');
    return btn.join(' | ');
}
function photoFormat(val, arr){
    if(val == '')return '未上传图片';
    return '<img src="http://img.51tz.com/'+val+'" width="80" style="margin-top:5px;" />';
}
//添加
function addCoupon(){
    var url = '<?php echo U('Coupon/editCouponFrame');?>';
    $('#coupon_coupon_edit_dialog').dialog({href:url});
    $('#coupon_coupon_edit_dialog').dialog('open');
}

//编辑
function couponEdit(id){
    if(typeof(id) !== 'number'){
        $.messager.alert('提示信息', '未选择数据', 'error');
        return false;
    }
    var url = "<?php echo U('Coupon/editCouponFrame');?>";
    url += url.indexOf('?') != -1 ? '&id='+id : '?id='+id;

    $('#coupon_coupon_edit_dialog').dialog({href:url});
    $('#coupon_coupon_edit_dialog').dialog('open');
}
//删除
function couponDelete(id){
    var ids = [];
    if(!id){
        var obj = $('#'+coupon_couponlist_datagrid_id).datagrid('getSelections');
        if(obj) for(var i = 0; i < obj.length; i++) ids.push(obj[i].id);
    }else{
        if(typeof(id) == 'number') ids.push(id);
    }
    if(ids.length == 0){
        $.messager.alert('提示信息', '未选择数据', 'error');
        return false;
    }

    $.messager.confirm('提示信息', '确定要删除吗？', function(result){
        alert('还是不允许删除');return false;
        if(!result) return false;
        $.post('<?php echo U('Coupon/couponDelete');?>', {ids: ids}, function(res){
            if(!res.status){
                $.messager.alert('提示信息', res.info, 'error');
            }else{
                $.messager.alert('提示信息', res.info, 'info');
                $('#'+coupon_couponlist_datagrid_id).datagrid('reload');
            }
        }, 'json');
    });
}
//排序
function couponDisplayOrder(){
    $.post('<?php echo U('Coupon/displayOrder');?>', $('.coupon_gridlist_order_input').serialize(), function(res){
        if(!res.status){
            $.messager.alert('提示信息', res.info, 'error');
        }else{
            $.messager.alert('提示信息', res.info, 'info');
            $('#'+coupon_couponlist_datagrid_id).datagrid('reload');
        }
    }, 'json');
}
//刷新
function couponCouponListRefresh(){
    $('#'+coupon_couponlist_datagrid_id).datagrid('reload');
}
</script>