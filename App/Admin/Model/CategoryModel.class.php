<?php
namespace Admin\Model;
use Think\Model;

class CategoryModel extends Model{
	protected $tableName = 'category';
	protected $pk        = 'id';
	
	//获取栏目列表
	public function getTree($parentid = 0){
		$field = array('id','`category_name`','description','display_order','`id` as `operateid`', 'status');
		$order = '`display_order` DESC,`id` DESC';
		$data = $this->field($field)->where(array('parent_id'=>$parentid))->order($order)->select();
		if (is_array($data)){
			foreach ($data as &$arr){
				$arr['children'] = $this->getTree($arr['id']);
			}
		}else{
			$data = array();
		}
		return $data;
	}
	
	//栏目下拉列表
	public function getSelectTree($parentid = 0){
		$field = array('`id`','`category_name` as `text`');
		$order = '`display_order` ASC,`id` DESC';
		$data = $this->field($field)->where(array('parent_id'=>$parentid))->order($order)->select();
		if (is_array($data)){
			foreach ($data as &$arr){
				$arr['children'] = $this->getSelectTree($arr['id']);
			}
		}else{
			$data = array();
		}
		return $data;
	}
	
	//内容管理右侧导航
	public function getCatTree($parentid = 0){
		$field = array('id','`category_name` as `text`');
		$order = '`display_order` ASC,`id` DESC';
		$data = $this->field($field)->where(array('parent_id'=>$parentid))->order($order)->select();
		if (is_array($data)){
			foreach ($data as $k=>&$arr){
				$arr['children'] = $this->getCatTree($arr['id']);
			}
		}else{
			$data = array();
		}
		return $data;
	}
}
