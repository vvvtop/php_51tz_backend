<?php
namespace Admin\Model;
use Think\Model;

class BrandModel extends Model{
    protected $tableName   = 'brand';
	protected $pk          = 'id';
    protected $tablePrefix = 'tz_';
}
