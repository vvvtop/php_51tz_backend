<?php
namespace Admin\Model;
use Think\Model;

class CouponModel extends Model{
    protected $tableName   = 'coupon';
	protected $pk          = 'id';
    protected $tablePrefix = 'tz_';
}
