<?php
namespace Admin\Model;
use Think\Model;

class DictOptionModel extends Model{
    protected $tableName   = 'dict_option';
	protected $pk          = 'id';
    protected $tablePrefix = 'tz_';
}
