<?php
namespace Admin\Model;
use Think\Model;

class DictModel extends Model{
    protected $tableName   = 'dict';
	protected $pk          = 'id';
    protected $tablePrefix = 'tz_';
}
