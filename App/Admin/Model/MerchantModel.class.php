<?php
namespace Admin\Model;
use Think\Model;

class MerchantModel extends Model{
    protected $tableName   = 'merchant';
	protected $pk          = 'id';
    protected $tablePrefix = 'tz_';
}
