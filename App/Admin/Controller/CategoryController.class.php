<?php
namespace Admin\Controller;
use Admin\Controller\CommonController;

/**
 * 栏目相关模块
 * @author wangdong
 */
class CategoryController extends CommonController {
    /**
     * 栏目管理
     */
    public function categoryList(){
        if(IS_POST){
            $categoryDB = D('Category');
            $data = $categoryDB->getTree();
            $this->ajaxReturn($data);
        }else{
            $menuDB = D('Menu');
            $currentpos = $menuDB->currentPos(I('get.menuid'));  //栏目位置
            $treegrid = array(
                'options'       => array(
                    'title'     => $currentpos,
                    'url'       => U('Category/categoryList', array('grid'=>'treegrid')),
                    'idField'   => 'id',
                    'treeField' => 'category_name',
                    'toolbar'   => 'category_categorylist_treegrid_toolbar',
                ),
                'fields' => array(
                    '排序'    => array('field'=>'display_order','width'=>15,'align'=>'center','formatter'=>'categoryCategoryListOrderFormatter'),
                    '栏目ID'  => array('field'=>'id','width'=>15,'align'=>'center'),
                    '栏目名称' => array('field'=>'category_name','width'=>50),
                    '描述'    => array('field'=>'description','width'=>50),
                    '状态'    => array('field'=>'status','width'=>15,'formatter'=>'categoryCategoryListStatusFormatter'),
                    '管理操作' => array('field'=>'operateid','width'=>50,'align'=>'center','formatter'=>'categoryCategoryListOperateFormatter'),
                )
            );
            $this->assign('treegrid', $treegrid);
            $this->display('category_list');
        }
    }

    /**
     * 添加栏目
     */
    public function categoryAdd(){
        if(IS_POST){
            $categoryDB = D('Category');
            $data = I('post.info');
            $id = $categoryDB->add($data);
            if($id){
                $this->success('添加成功');
            }else {
                $this->error('添加失败');
            }
        }else{
            $this->display('category_add');
        }
    }

    /**
     * 编辑栏目
     */
    public function categoryEdit($id){
        $categoryDB = D('Category');
        if(IS_POST){
            $data = I('post.info');
            $res = $categoryDB->where(array('id'=>$id))->save($data);
            if($res){
                $this->success('操作成功');
            }else {
                $this->error('操作失败');
            }
        }else{
            $info = $categoryDB->where(array('id'=>$id))->find();
            $this->assign('info', $info);
            $this->display('category_edit');
        }
    }

    /**
     * 删除栏目
     */
    public function categoryDelete($id = 0){
        if($id && IS_POST){
            $categoryDB = D('Category');
            $result = $categoryDB->where(array('id'=>$id))->delete();
            if($result){
                $this->success('删除成功');
            }else {
                $this->error('删除失败');
            }
        }else{
            $this->error('删除失败');
        }
    }

    /**
     * 栏目排序
     */
    public function categoryOrder(){
        if(IS_POST) {
            $categoryDB = D('Category');
            foreach(I('post.order') as $id => $order) {
                $categoryDB->where(array('id'=>$id))->save(array('display_order'=>$order));
            }
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }

    /**
     * 栏目下拉框
     */
    public function getEasyCategory(){
        $categoryDB = D('Category');
        $data = $categoryDB->getSelectTree();
        $this->ajaxReturn($data);
    }
}
