<?php
namespace Admin\Controller;
use Admin\Controller\CommonController;

/**
 * 优惠券管理模块
 * @author gaosk<gaoshikao@qq.com>
 */
class CouponController extends CommonController {
    /**
     * 优惠券列表
     */
    public function couponList($page = 1, $rows = 20, $search = array(), $sort = 'display_order', $order = 'desc'){
        if(IS_POST){
            $couponDB = D('Coupon');
            $cityDB = D('City');
            $brandDB = D('Brand');
            $merchantDB = D('Merchant');
            $categoryDB = M('category');
            $dictOptionDB = D('DictOption');

            $where = array();
            if(session('roleid') != 1){
                $where['city_id'] = array('in', session('cityid'));
            }

            $limit=($page - 1) * $rows . "," . $rows;
            $total = $couponDB->where($where)->count();
            $order = $sort.' '.$order;

            $field = 'Co.id,Co.amount,Co.expire_time,Co.city_id,Co.brand_id,Co.merchant_id,Co.display_order,Co.status,' .
                'Co.category_id,Co.coupon_type,Co.photo,C.city_name,M.merchant_name,B.brand_name,' .
                'Co.id as coupon_id';
            $couponList = array();
            if($total > 0){
                $couponList = $couponDB->table($couponDB->getTableName() . ' Co')
                    ->join('left join '.$cityDB->getTableName() . ' C on C.id = Co.city_id')
                    ->join('left join '.$brandDB->getTableName() . ' B on B.id = Co.brand_id')
                    ->join('left join '.$merchantDB->getTableName() . ' M on M.id = Co.merchant_id')
                    ->where($where)->field($field)->order($order)->limit($limit)->select();
            }
            foreach($couponList as $k => $c){
                $couponList[$k]['expire_time'] = date('Y-m-d', $c['expire_time']);
                $couponList[$k]['category_name'] = $categoryDB->where(array('id'=>array('eq', $c['category_id'])))->getField('category_name');
                $couponList[$k]['coupon_type_name'] = $dictOptionDB->where(array('field_value'=>array('eq', $c['coupon_type']),'dict_id'=>array('eq', 2)))
                    ->getField('field_name');
            }

            $data = array('total'=>$total, 'rows'=>$couponList);
            $this->ajaxReturn($data);
        }else{
            $menu_db = D('Menu');
            $currentpos = $menu_db->currentPos(I('get.menuid'));
            $datagrid = array(
                'options'       => array(
                    'title'     => $currentpos,
                    'url'       => U('Coupon/couponList', array('grid'=>'datagrid')),
                    'toolbar'   => 'coupon_couponlist_datagrid_toolbar',
                    'singleSelect' => false,
                    'width'     => '600'
                ),
                'fields' => array(
                    '选中'    => array('field'=>'ck', 'checkbox'=>true),
                    '排序'     => array('field'=>'display_order','width'=>8,'formatter'=>'displayOrderFormat','sortable'=>true),
                    'ID'      => array('field'=>'id','width'=>10,'sortable'=>true),
                    '图片'      => array('field'=>'photo','width'=>20,'sortable'=>true,'formatter'=>'photoFormat'),
                    '类别'   => array('field'=>'coupon_type_name','width'=>15,'sortable'=>true),
                    '金额'     => array('field'=>'amount','width'=>15,'sortable'=>false),
                    '到期时间'   => array('field'=>'expire_time','width'=>20,'sortable'=>false),
                    '城市'   => array('field'=>'city_name','width'=>15,'sortable'=>true),
                    '品牌'   => array('field'=>'brand_name','width'=>20,'sortable'=>true),
                    '商家'   => array('field'=>'merchant_name','width'=>20,'sortable'=>true),
                    '建材'   => array('field'=>'category_name','width'=>15,'sortable'=>true),
                    '状态'    => array('field'=>'status','width'=>10,'sortable'=>true,'formatter'=>'statusFormat'),
                    '管理操作' => array('field'=>'coupon_id','width'=>20,'formatter'=>'operateFormat')
                )
            );
            $this->assign('datagrid', $datagrid);
            $this->display('coupon_list');
        }
    }

    /**
     * 编辑优惠券
     */
    public function editCouponFrame($id = 0){
        $couponDB = D('Coupon');
        if(IS_POST){
            $data = I('post.info');
            if(!isset($data['status'])){
                $data['status'] = 0;
            }
            $data['expire_time'] = strtotime($data['expire_time']);
            $id = intval($id);
            if($id > 0){
                $res = $couponDB->where(array('id'=>$id))->save($data);
            }else{
                $res = $couponDB->add($data);
            }
            if($res){
                $this->success('操作成功');
            }else {
                $this->error('操作失败');
            }
        }else{
            if($id > 0){
                $info = $couponDB->where(array('id'=>$id))->find();
                if($info['expire_time'] > 0){
                    $info['expire_time'] = date('Y-m-d', $info['expire_time']);
                }else{
                    $info['expire_time'] = '';
                }

            }else{
                $info = array('id' => 0);
            }
            $this->assign('info', $info);
            $this->display('coupon_edit');
        }
    }

    /**
     * 删除优惠券
     */
    public function couponDelete($id = 0){
        $this->error('优惠券无法删除，请联系管理员');
    }

    /**
     * 优惠券排序
     */
    public function displayOrder(){
        if(IS_POST) {
            $couponDB = D('Coupon');
            foreach(I('post.order') as $id => $order) {
                $couponDB->where(array('id'=>$id))->save(array('display_order'=>$order));
            }
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }

}
