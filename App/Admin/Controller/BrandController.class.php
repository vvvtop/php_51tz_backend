<?php
namespace Admin\Controller;
use Admin\Controller\CommonController;

/**
 * 城市及分站管理模块
 * @author gaosk<gaoshikao@qq.com>
 */
class BrandController extends CommonController {
    /**
     * 城市列表
     */
    public function brandList($page = 1, $rows = 20, $search = array(), $sort = 'display_order', $order = 'desc'){
        if(IS_POST){
            $brandDB = M('Brand');
            $brandCategoryDB = M('BrandCategory');
            $categoryDB = M('Category');

            $where = array();

            if(!empty($search['brand_name'])){
                $where['brand_name'] = array('like', $search['brand_name'].'%');
            }
            $limit=($page - 1) * $rows . "," . $rows;
            $total = $brandDB->where($where)->count();
            $order = $sort.' '.$order;
            $field = 'id,brand_name,logo,id as brand_id,status,display_order';
            $list = $total ? $brandDB->where($where)->field($field)->order($order)->limit($limit)->select() : array();
            foreach($list as $k => $l){
                $categoryIds = $brandCategoryDB->where(array('brand_id'=>array('eq', $l['id'])))->getField('group_concat(category_id)');
                $list[$k]['category_names'] = $categoryDB->where(array('id'=>array('in', $categoryIds)))->getField('group_concat(category_name)');
            }
            $data = array('total'=>$total, 'rows'=>$list);
            $this->ajaxReturn($data);
        }else{
            $menu_db = D('Menu');
            $currentpos = $menu_db->currentPos(I('get.menuid'));
            $datagrid = array(
                'options'       => array(
                    'title'     => $currentpos,
                    'url'       => U('Brand/brandList', array('grid'=>'datagrid')),
                    'toolbar'   => 'brand_brandlist_datagrid_toolbar',
                    'singleSelect' => false,
                    'width'     => '600'
                ),
                'fields' => array(
                    '选中'    => array('field'=>'ck', 'checkbox'=>true),
                    '排序'     => array('field'=>'display_order','width'=>5,'formatter'=>'displayOrderFormat','sortable'=>true),
                    'ID'      => array('field'=>'id','width'=>5,'sortable'=>true),
                    '名称'     => array('field'=>'brand_name','width'=>15,'sortable'=>false),
                    '建材类别' => array('field'=>'category_names','width'=>50),
                    'Logo'   => array('field'=>'logo','width'=>20,'formatter'=>'photoFormat','sortable'=>false),
                    '审核状态'    => array('field'=>'status','width'=>10,'sortable'=>true,'formatter'=>'statusFormat'),
                    '管理操作' => array('field'=>'brand_id','width'=>25,'formatter'=>'operateFormat')
                )
            );
            $this->assign('datagrid', $datagrid);
            $this->display('brand_list');
        }
    }

    public function getEasyBrand(){
        $where = array();
        $brandDB = D('Brand');
        $brandList = $brandDB->where($where)->order('display_order desc')->field('id,brand_name as text')->select();
        $this->ajaxReturn($brandList);
    }

    /**
     * 编辑城市
     */
    public function editBrandFrame($id = 0){
        $brandDB = D('Brand');
        $brandCategoryDB = M('BrandCategory');
        if(IS_POST){
            $data = I('post.info');
            $categoryIdArr = $data['category_ids'];
            unset($data['category_ids']);
            
            //品牌对应分类处理
            if($id > 0){
                $brandCategoryDB->where(array('brand_id' => array('eq', $id)))->delete();
            }
            $brandCategoryList = array();

            if(!isset($data['status'])){
                $data['status'] = 0;
            }
            $data['album'] = strval(implode(',', $data['album']));
            if($id > 0){
                $res = $brandDB->where(array('id'=>$id))->save($data);
            }else{
                $res = $id = $brandDB->add($data);
            }
            
            if($id > 0){
                foreach($categoryIdArr as $categoryId){
                    $brandCategoryList[] = array('brand_id'=>$id, 'category_id'=>$categoryId);
                }
                $res = $brandCategoryDB->addAll($brandCategoryList);
            }

            if($res){
                $this->success('操作成功');
            }else {
                $this->error('操作失败');
            }
        }else{
            if($id > 0){
                $info = $brandDB->where(array('id'=>$id))->find();
                $info['category_ids'] = $brandCategoryDB->where(array('brand_id' => array('eq', $id)))->getField('group_concat(category_id)');
            }else{
                $info = array('id' => 0);
            }
            $this->assign('info', $info);
            $this->display('brand_edit');
        }
    }

    /**
     * 删除城市
     */
    public function brandDelete($id = 0){
        $this->error('城市无法删除，请联系管理员');
    }

    /**
     * 栏目排序
     */
    public function displayOrder(){
        if(IS_POST) {
            $brandDB = D('Brand');
            foreach(I('post.order') as $id => $order) {
                $brandDB->where(array('id'=>$id))->save(array('display_order'=>$order));
            }
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }

}
