<?php
namespace Admin\Controller;
use Admin\Controller\CommonController;

/**
 * 城市及分站管理模块
 * @author gaosk<gaoshikao@qq.com>
 */
class DictController extends CommonController {
	/**
	 * 城市列表
	 */
	public function cityList($page = 1, $rows = 20, $search = array(), $sort = 'display_order', $order = 'desc'){
		if(IS_POST){
    		$cityDB = M('City');
            
            $where = array();
            if(session('roleid') != 1){
                $where['id'] = array('in', session('cityid'));
            }
            
            if(!empty($search['city_name'])){
                $where['city_name'] = array('like', $search['city_name'].'%');
            }
            $limit=($page - 1) * $rows . "," . $rows;
            $total = $cityDB->where($where)->count();
            $order = $sort.' '.$order;
            $field = 'id,city_name,domain,pinyin,first_char,open_status,display_order,id as city_id';
            $list = $total ? $cityDB->where($where)->field($field)->order($order)->limit($limit)->select() : array();
            $data = array('total'=>$total, 'rows'=>$list);
            $this->ajaxReturn($data);
		}else{
            $menu_db = D('Menu');
            $currentpos = $menu_db->currentPos(I('get.menuid'));
			$datagrid = array(
		        'options'       => array(
                    'title'     => $currentpos,
    				'url'       => U('City/cityList', array('grid'=>'datagrid')),
    				'toolbar'   => 'city_citylist_datagrid_toolbar',
                    'singleSelect' => false,
                    'width'     => '600'
    			),
                'fields' => array(
                    '选中'    => array('field'=>'ck', 'checkbox'=>true),
                    '排序'     => array('field'=>'display_order','width'=>5,'formatter'=>'displayOrderFormat','sortable'=>true),
                    'ID'      => array('field'=>'id','width'=>10,'sortable'=>true),
                    '名称'     => array('field'=>'city_name','width'=>15,'sortable'=>false),
                    '域名'   => array('field'=>'domain','width'=>15,'sortable'=>false),
                    '拼音'   => array('field'=>'pinyin','width'=>15,'sortable'=>true),
                    '开通状态'    => array('field'=>'open_status','width'=>10,'sortable'=>true,'formatter'=>'openStatusFormat'),
                    '管理操作' => array('field'=>'city_id','width'=>25,'formatter'=>'operateFormat')
                )
		    );
		    $this->assign('datagrid', $datagrid);
			$this->display('city_list');
		}
	}

    public function getEasyDict($var){
        $where = array();
        $dictDB = D('Dict');
        $optionDB = D('DictOption');
        $dictId = $dictDB->where(array('variable_name' => array('eq', $var)))->getField('id');
        $optionList = $optionDB->where(array('dict_id' => array('eq', $dictId)))->order('display_order desc')->field('field_value as id,field_name as text')->select();
        $this->ajaxReturn($optionList);
    }
	/**
	 * 添加城市
	 */
	public function addCityFrame(){
		if(IS_POST){
			$cityDB = D('City');
			$data = I('post.info');
    		$id = $cityDB->add($data);
    		if($id){
    			$this->success('添加成功');
    		}else {
    			$this->error('添加失败');
    		}
		}else{
			$this->display('city_add');
		}
	}

    /**
     * 编辑城市
     */
    public function editDomainFrame($id){
        $cityDB = D('City');
        if(IS_POST){
            $data = I('post.info');
            if(!isset($data['open_status'])){
                $data['open_status'] = 0;
            }
            $res = $cityDB->where(array('id'=>$id))->save($data);
            if($res){
                $this->success('操作成功');
            }else {
                $this->error('操作失败');
            }
        }else{
            $info = $cityDB->where(array('id'=>$id))->find();
            $this->assign('info', $info);
            $this->display('city_domain');
        }
    }
   
	/**
	 * 编辑城市
	 */
	public function editCityFrame($id){
		$cityDB = D('City');
		if(IS_POST){
			$data = I('post.info');
            if(!isset($data['show_customer'])){
                $data['show_customer'] = 0;
            }
    		$res = $cityDB->where(array('id'=>$id))->save($data);
    		if($res){
    			$this->success('操作成功');
    		}else {
    			$this->error('操作失败');
    		}
		}else{
			$info = $cityDB->where(array('id'=>$id))->find();
			$this->assign('info', $info);
			$this->display('city_edit');
		}
	}
	
	/**
	 * 删除城市
	 */
	public function cityDelete($id = 0){
        $this->error('城市无法删除，请联系管理员');
	}
	
	/**
	 * 栏目排序
	 */
	public function displayOrder(){
		if(IS_POST) {
			$cityDB = D('City');
			foreach(I('post.order') as $id => $order) {
				$cityDB->where(array('id'=>$id))->save(array('display_order'=>$order));
			}
			$this->success('操作成功');
		} else {
			$this->error('操作失败');
		}
	}	
    
}
