<?php
namespace Admin\Controller;
use Admin\Controller\CommonController;

/**
 * 城市及分站管理模块
 * @author gaosk<gaoshikao@qq.com>
 */
class MerchantController extends CommonController {
	/**
	 * 城市列表
	 */
	public function merchantList($page = 1, $rows = 20, $search = array(), $sort = 'display_order', $order = 'desc', $brand_id = 0){
		if(IS_POST){
    		$merchantDB = M('Merchant');
            $brandMerchantDB = M('BrandMerchant');
            $brandDB = M('Brand');

            $where = array();
            if(session('roleid') != 1){
                $where['city_id'] = array('in', session('cityid'));
            }
            if($brand_id > 0){
                $merchantIds = $brandMerchantDB->where(array('brand_id' => array('eq', $brand_id)))->getField('group_concat(merchant_id)');
                $where['id'] = array('in', $merchantIds);
            }

            if(!empty($search['merchant_name'])){
                $where['merchant_name'] = array('like', $search['merchant_name'].'%');
            }
            $limit=($page - 1) * $rows . "," . $rows;
            $total = $merchantDB->where($where)->count();
            $order = $sort.' '.$order;
            $field = 'id,merchant_name,telephone,address,id as merchant_id,status,display_order';
            $list = $total ? $merchantDB->where($where)->field($field)->order($order)->limit($limit)->select() : array();
            foreach($list as $k => $l){
                $brandIds = $brandMerchantDB->where(array('merchant_id'=>array('eq', $l['id'])))->getField('group_concat(brand_id)');
                $list[$k]['brand_names'] = $brandDB->where(array('id'=>array('in', $brandIds)))->getField('group_concat(brand_name)');
            }
            $data = array('total'=>$total, 'rows'=>$list);
            $this->ajaxReturn($data);
		}else{
            $menu_db = D('Menu');
            $currentpos = $menu_db->currentPos(I('get.menuid'));
			$datagrid = array(
		        'options'       => array(
                    'title'     => $currentpos,
    				'url'       => U('Merchant/merchantList', array('grid'=>'datagrid','brand_id'=>$brand_id)),
    				'toolbar'   => 'merchant_merchantlist_datagrid_toolbar',
                    'singleSelect' => false,
                    'width'     => '600'
    			),
                'fields' => array(
                    '选中'    => array('field'=>'ck', 'checkbox'=>true),
                    '排序'     => array('field'=>'display_order','width'=>5,'formatter'=>'displayOrderFormat','sortable'=>true),
                    'ID'      => array('field'=>'id','width'=>10,'sortable'=>true),
                    '名称'     => array('field'=>'merchant_name','width'=>15,'sortable'=>false),
                    '电话'   => array('field'=>'telephone','width'=>15,'sortable'=>false),
                    '地址'   => array('field'=>'address','width'=>20,'sortable'=>false),
                    '代理品牌'   => array('field'=>'brand_names','width'=>50,'sortable'=>false),
                    '状态'    => array('field'=>'status','width'=>10,'sortable'=>true,'formatter'=>'statusFormat'),
                    '管理操作' => array('field'=>'merchant_id','width'=>25,'formatter'=>'operateFormat')
                )
		    );
		    $this->assign('datagrid', $datagrid);
			$this->display('merchant_list');
		}
    }


    public function getEasyMerchant(){
        $where = array();
        $merchantDB = D('Merchant');
        $merchantList = $merchantDB->where($where)->order('id desc')->field('id,merchant_name as text')->select();
        $this->ajaxReturn($merchantList);
    }

	/**
	 * 编辑商家
	 */
	public function editMerchantFrame($id = 0){
		$merchantDB = D('Merchant');
        $brandMerchantDB = M('BrandMerchant');
        if(IS_POST){
            $data = I('post.info');
            $brandIdArr = $data['brand_ids'];
            unset($data['brand_ids']);

            if($id > 0){
                $brandMerchantDB->where(array('merchant_id' => array('eq', $id)))->delete();
            }
            $brandMerchantList = array();

            if(!isset($data['status'])){
                $data['status'] = 0;
            }
            if($id > 0){
                $res = $merchantDB->where(array('id'=>$id))->save($data);
            }else{
                $res = $id = $merchantDB->add($data);
            }

            if($id > 0){
                foreach($brandIdArr as $brandId){
                    $brandMerchantList[] = array('merchant_id'=>$id, 'brand_id'=>$brandId);
                }
                $res = $brandMerchantDB->addAll($brandMerchantList);
            }

    		if($res){
    			$this->success('操作成功');
    		}else {
    			$this->error('操作失败');
    		}
		}else{
            if($id > 0){
    			$info = $merchantDB->where(array('id'=>$id))->find();
                $info['brand_ids'] = $brandMerchantDB->where(array('merchant_id' => array('eq', $id)))->getField('group_concat(brand_id)');
            }else{
                $info = array('id'=>0);
            }
			$this->assign('info', $info);
			$this->display('merchant_edit');
		}
	}

	/**
	 * 删除商家
	 */
	public function merchantDelete($id = 0){
        $this->error('城市无法删除，请联系管理员');
	}

	/**
	 * 栏目排序
	 */
	public function displayOrder(){
		if(IS_POST) {
			$merchantDB = D('Merchant');
			foreach(I('post.order') as $id => $order) {
				$merchantDB->where(array('id'=>$id))->save(array('display_order'=>$order));
			}
			$this->success('操作成功');
		} else {
			$this->error('操作失败');
		}
	}

}
